# Satori2

Satori2 is a simple testing system which allows to run programming contests (ICPC style) and programming courses.

Satori2 was modeled on the https://satori.tcs.uj.edu.pl/

It was originally developed as a project for Data Engineering course at Theoretical Computer Science, Jagiellonian University.

Currently Satori2 supports only C++.

## Usage

See satori2/README.md

## Authors

Krzysztof Ziobro

Jacek Salata

## License
[MIT](https://choosealicense.com/licenses/mit/)