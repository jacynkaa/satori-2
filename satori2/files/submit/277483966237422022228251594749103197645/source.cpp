#include<bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
using namespace std;
using namespace __gnu_pbds;

#define endl "\n"
#define mp make_pair

typedef tree<int,null_type,less<int>,rb_tree_tag, tree_order_statistics_node_update> indexed_set; // find_by_order(x) <-returns x-th element   order_of_key(x) <- returns order of element x

const int milion=100000000;

struct drzewo_przedzialowe
{
    vector <int> X;
    int rozmiar;

    void stworz (int x)
    {
        rozmiar=1;
        while (x>rozmiar)
            rozmiar=rozmiar*2;

        X.resize(rozmiar*2,0);
    }

    void zmien (int q, int x)
    {
        q=q+rozmiar;
        while (q>0)
        {
            X[q]=max(X[q],x);
            q=q/2;
        }
    }

    int znajdz ( int q)
    {
        int qprim=q;
        int p=rozmiar;
        q=q+rozmiar;
        int odp=0;

        while (q>=p)
        {
            if(p%2==1)
            {
                odp=max(odp,X[p]);
                p++;
            }
            if(q%2==0)
            {
                odp=max(odp,X[q]);
                q--;
            }
            q=q/2;
            p=p/2;
        }
        zmien(qprim, odp+1);
    }

    int do_odp (int q)
    {
        int p=rozmiar;
        q=q+rozmiar;
        int odp=0;

        while (q>=p)
        {
            if(p%2==1)
            {
                odp=max(odp,X[p]);
                p++;
            }
            if(q%2==0)
            {
                odp=max(odp,X[q]);
                q--;
            }
            q=q/2;
            p=p/2;
        }
        return odp;
    }
};

main()
{
    ios_base::sync_with_stdio(false);
    int n,x;
    cin>>n>>x;
 //   cout <<"ok"<<endl;
    int odp=0;

    vector <pair<int, int> > X;

    for (int i=0; i<n; i++)
    {
        int a;
        cin>>a;
        auto c=mp(a,-i);
        X.push_back(c);
    }
   // cout <<"ok"<<endl;

    for (int i=0; i<n; i++)
    {
        auto c=X[i];
        c.first=c.first+x;
        c.second=c.second-2*milion;
        X.push_back(c);
    }
   // cout <<"ok"<<endl;

    sort(X.begin(), X.end());
    // return 0;

    drzewo_przedzialowe normalne;
    drzewo_przedzialowe odwrocone;

    normalne.stworz(n);
    odwrocone.stworz(n);

    //cout <<odwrocone.rozmiar<<endl;
  //  for (int i=0; i<X.size(); i++)
  //  cout <<X[i].first<<" "<<X[i].second<<endl;

  //  cout <<"ok"<<endl;
//

    for (int i=X.size()-1; i>=0; i--)
        if(X[i].second>-milion)
        {
      //  cout <<n-1+X[i].second<<endl;
            odwrocone.znajdz(n-1+X[i].second);
        }


    for (int i=0; i<X.size(); i++)
    {
        if(X[i].second>-milion)
            normalne.znajdz(-X[i].second);
        else
        {

    //    cout <<odwrocone.rozmiar+n-1+X[i].second+2*milion<<" "<<-X[i].second-2*milion<<endl;
           int r=odwrocone.X[odwrocone.rozmiar+n-1+X[i].second+2*milion];
           int s=normalne.do_odp(-X[i].second-2*milion-1);
        //cout <<X[i].first<<" "<<X[i].second<<" "<<r<<" "<<s<<endl;
    odp=max(odp, r+s);
        }
    }
    cout <<odp<<endl;
}


