#include<bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include<math.h>
//#pragma GCC optimize ("-O3")
using namespace std;
using namespace __gnu_pbds;

#define endl "\n"
#define mp make_pair
#define double long double
#define fi first
#define se second

typedef tree<int,null_type,less<int>,rb_tree_tag, tree_order_statistics_node_update> indexed_set; // find_by_order(x) <-returns x-th element   order_of_key(x) <- returns order of element x

int liczby [25];
int n,k;
vector <int> X;
int sum, ile_zer;
long long int wypisz=0;
long long int mnoznik=1;

void wczytywanie ()
{
    wypisz=0;
    mnoznik=1;
    cin>>n>>k;
    for (int i=0; i<n; i++)
        cin>>liczby[i];
    for (long long int i=1; i<= (((k+1)/2))-1; i++)
        mnoznik*=i;
    for (long long int i=1; i<=(k/2); i++)
        mnoznik*=i;
}

inline int wartosc (int x)
{
    int odp=0;

    for (int i=0; i<n; i++)
        if((x&(1<<i))!=0)
            odp+=liczby[i];
    return odp;
}

inline int zera (int x)
{
    int odp=0;
    for (int i=0; i<n; i++)
        if((x&(1<<i))!=0)
            if(liczby[i]==0)
                odp++;
    return odp;
}

void generator_podzbiorow_pomocniczy (int ktora_najwieksza, int postac_liczbowa, int dlugosc, int suma, int ile_zer)
{
    if(dlugosc==((k+1)>>1))
    {
        if(((sum-2*suma+1111)%11)==0)
        {
            //  cout <<postac_liczbowa<<endl;
            long long int q=1;
            int r=ile_zer;
            q*=((((k+1)/2))-r);
            wypisz+=q;
        }
        return;
    }
    for (int i=ktora_najwieksza+1; i<X.size(); i++)
        generator_podzbiorow_pomocniczy(i, postac_liczbowa+(1<<X[i]), dlugosc+1, suma+liczby[X[i]], ile_zer+(liczby[X[i]]==0));
    return;
}

void akcja (int x)
{
    X.clear();
    for (int i=0; i<n; i++)
        if((x&(1<<i))!=0)
            X.push_back(i);

    sum=wartosc(x);
    ile_zer=zera(x);
    generator_podzbiorow_pomocniczy(-1,0,0,0,0);
}

void generator_podzbiorow (int ktora_najwieksza, int postac_liczbowa)
{
    if(__builtin_popcount(postac_liczbowa)==k)
    {
        akcja(postac_liczbowa);
        return;
    }
    for (int i=ktora_najwieksza+1; i<n; i++)
        generator_podzbiorow(i, postac_liczbowa+(1<<i));
    return;
}

main()
{

    ios_base::sync_with_stdio(false);
    cout.precision(10);
    cout.setf(ios::fixed);

    int Z;
    Z=1;
    while (Z--)
    {
        //cout <<"nowy"<<endl;
        wczytywanie();
        if(k==1)
        {
            cout <<0<<endl;
            continue;
        }
        if(k>n)
         {
            cout <<0<<endl;
            continue;
        }

        generator_podzbiorow(-1,0);
        cout <<wypisz*mnoznik<<endl;//" "<<qq<<endl;
    }
}
