## MANUAL:

**Python >=3.6 and PostgresSQL required!**

To install all packages run:

pip3  install -r REQUIREMENTS

Configure PostgresSQL database - create database satori2 owned by user satori2 with password: 'satori-2019'.
Do it manually or just run:

sudo -u postgres psql < db_setup.sql

Details of database configuration are in satori2/settings.py in DATABASE section and can be changed if needed (e.g. database host is not localhost)

Then run:

./setup.sh

You will be prompted to create superuser. Fill in the blanks (e-mail address doesn't need to be real, just valid) and finally run:

python3 manage.py runserver

If everything is OK you should see something like that:

![Screenshot1](scr/4.png)

Open http://127.0.0.1:8000/admin in your browser and log in with your superuser account.

At http://127.0.0.1:8000/admin you can manage database via admin site. It is the only way to create contests or grant admin privilege to user.
Add new contest and then add new contest_user - select role, user and contest form the list.


![Screenshot1](scr/1.png)

Then open the main site: http://127.0.0.1:8000/account and log in or create a new account.

![Screenshot1](scr/2.png)

After logging in you can select contest to enter. (You see only the ones you manage or joined)

Contest interface is really simple and very similar to the Satori's one.

![Screenshot1](scr/3.png)

Satori2 is compatible with Satori so you can add problems from original Satori (currently Satori2 supports only C++):
Tests: \*.zip (\*.in and \*.out files), statement: \*.pdf and checker: \*.cpp (if empty default checker is: "diff -wB")
or add one of the problems we prepared (example/[problem])

User answers are tested using: "./checker x.in x.out user_output" command, where "x" is the test name. It should return 0 iff user_output is correct. Details of tester are in account/tester.py.

If you have any problems or questions don't hesitate and contact me: jacek.salata@student.uj.edu.pl

## ADDITIONAL INFO:

In order to clean the db drop user and database you created during configuration: 

sudo -u postgres psql < db_clear.sql


Testing script requires access to:
basic bash commands
unzip
/usr/bin/time
g++ with c++14













