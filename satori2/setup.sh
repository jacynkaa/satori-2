#!/usr/bin/env bash

python3 manage.py migrate
python3 manage.py makemigrations account
python3 manage.py migrate
python3 manage.py dbshell < account/ranking.sql
python3 manage.py createsuperuser