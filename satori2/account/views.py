from django.core.exceptions import PermissionDenied
from django.http import *
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from .dbq import *
from .tester import *
import _thread
import os


@login_required
def dashboard(request):
    contest = get_contests(request.user)
    return render(request, 'account/dashboard.html', {'section': 'dashboard', 'contest': contest})


@login_required
def contest_detail(request, contest_id):

    if not does_exist_contest(contest_id):
        raise Http404
    if not has_permission_contest(request.user, contest_id):
        raise PermissionDenied

    contest = get_object_or_404(Contest, id=contest_id)
    name = get_object_or_404(Contest, id=contest_id).name

    return render(request, 'account/contest.html', {'section': 'contest', 'contest_id': contest.id,
                                                    'name': name, 'is_admin': is_admin(request.user, contest_id),'contestname':contest_name(contest_id),})


@login_required
def problem(request, contest_id):

    if not does_exist_contest(contest_id):
        raise Http404
    if not has_permission_contest(request.user, contest_id):
        raise PermissionDenied

    problems = get_problems(request.user,contest_id)
    return render(request, 'account/problem.html', {'section': 'problem', 'contest_id': contest_id,
                                                    'problems': problems, 'is_admin': is_admin(request.user, contest_id),'contestname':contest_name(contest_id),})


def register(request):
    if request.method == 'POST':
        user_form = UserRegistrationForm(request.POST)
        if user_form.is_valid():
            new_user = user_form.save(commit=False)
            new_user.set_password(user_form.cleaned_data['password'])
            new_user.save()
            return render(request, 'account/register_done.html', {'new_user': new_user})
    else:
        user_form = UserRegistrationForm()
        for f in user_form.fields:
            user_form.fields[f].required=True

    return render(request, 'account/register.html', {'user_form': user_form})


@login_required
def download_problem(request,  contest_id, problem_id):

    if not does_exist_problem(contest_id, problem_id):
        raise Http404
    if not has_permission_problem(request.user, contest_id, problem_id):
        raise PermissionDenied

    path = get_object_or_404(Problem, id=problem_id).statement.path
    if os.path.exists(path):
        with open(path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/pdf")
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(path)
            return response
    raise Http404


@login_required
def add_problem(request, contest_id):

    if not does_exist_contest(contest_id):
        raise Http404
    if not has_permission_contest(request.user, contest_id) or not is_admin(request.user, contest_id):
        raise PermissionDenied

    if request.method == 'POST':
        problem_form = ProblemRegistrationForm(request.POST, request.FILES)
        if problem_form.is_valid():
            new_problem = problem_form.save(commit=False)
            new_problem.contest = get_object_or_404(Contest, id=contest_id)
            new_problem.save()
            return render(request, 'account/add_problemdone.html', {'section': 'add_problem', 'problem_form': problem_form,
                                                                    'contest_id': contest_id, 'is_admin': is_admin(request.user, contest_id),'contestname':contest_name(contest_id),})
    else:
        problem_form = ProblemRegistrationForm()
    return render(request, 'account/add_problem.html', {'section': 'add_problem', 'problem_form': problem_form,
                                                        'contest_id': contest_id, 'is_admin': is_admin(request.user, contest_id),'contestname':contest_name(contest_id),})


@login_required
def edit_problem(request,  contest_id, problem_id):

    if not does_exist_problem(contest_id, problem_id):
        raise Http404
    if not has_permission_problem(request.user, contest_id, problem_id) or not is_admin(request.user, contest_id):
        raise PermissionDenied

    problem = get_object_or_404(Problem, id=problem_id)
    if request.method == 'POST':
        problem_form = ProblemRegistrationForm(request.POST, request.FILES, instance=problem)
        if problem_form.is_valid():
            edited_problem = problem_form.save(commit=False)
            edited_problem.id = problem_id
            edited_problem.contest = get_object_or_404(Contest, id=contest_id)
            edited_problem.save()
            return render(request, 'account/edit_problem_done.html', {'problem_form': problem_form, 'contest_id': contest_id,
                                                                      'problem_id': str(problem_id), 'is_admin': is_admin(request.user, contest_id),'contestname':contest_name(contest_id),})
    else:
        problem_form = ProblemRegistrationForm(instance=problem)
    return render(request, 'account/edit_problem.html', {'problem_form': problem_form, 'contest_id': contest_id,
                                                         'is_admin': is_admin(request.user, contest_id),'contestname':contest_name(contest_id),})


@login_required
def add_contest_user(request,  contest_id):

    if not does_exist_contest(contest_id):
        raise Http404
    if not has_permission_contest(request.user, contest_id) or not is_admin(request.user, contest_id):
        raise PermissionDenied

    if request.method == 'POST' and 'add' in request.POST:

        form = ContestUserForm(request.POST)
        if form.is_valid():
            new_contest_user = form.save(commit=False)
            new_contest_user.contest = get_object_or_404(Contest, id=contest_id)
            new_contest_user.save()
            return render(request, 'account/add_contest_user_done.html', {'section': 'add_contest_user','form': form,
                                                                          'contest_id': contest_id, 'is_admin': is_admin (request.user, contest_id),'contestname':contest_name(contest_id),})
    else:
        form = ContestUserForm()
    if request.method == 'POST' and 'del' in request.POST:

        delete_form = ContestUserForm(request.POST)
        if delete_form.is_valid():
            del_contest_user = delete_form.save(commit=False)
            delete_user_from_contest(del_contest_user.user, contest_id)
            return render(request, 'account/delete_contest_user_done.html', {'section': 'add_contest_user','form': form,
                                                                          'contest_id': contest_id, 'is_admin': is_admin (request.user, contest_id),'contestname':contest_name(contest_id),})
    else:
        delete_form = DeleteContestUserForm()

    form.fields['user'].queryset = get_not_users_nor_admins(contest_id)
    delete_form.fields['user'].queryset = get_users(contest_id)
    return render(request, 'account/add_contest_user.html', {'section': 'add_contest_user','form': form,
                                                             'contest_id': contest_id, 'users': get_users(contest_id),
                                                             'admins': get_admins(contest_id), 'is_admin': is_admin (request.user, contest_id), 'del':delete_form,'contestname':contest_name(contest_id),})


@login_required
def submit(request, contest_id):

    if not does_exist_contest(contest_id):
        raise Http404
    if not has_permission_contest(request.user, contest_id):
        raise PermissionDenied

    if request.method == 'POST':
        form = SubmitForm(request.POST, request.FILES)
        if form.is_valid():
            new_submit = form.save(commit=False)
            new_submit.user = request.user
            saved = False
            try:
                new_submit.save()
                saved = True
            except ValidationError as e:
                form.add_error('problem',e.args[0])
            except Exception as e:
                raise e
            if saved:
                _thread.start_new_thread(sub,(new_submit,))
                return render(request, 'account/submit_done.html', {'section': 'submit','form': form, 'contest_id': contest_id,
                                                                   'is_admin': is_admin(request.user, contest_id),'contestname':contest_name(contest_id),})
    else:
        form = SubmitForm()
    form.fields['problem'].queryset= get_problems(request.user, contest_id)
    return render(request, 'account/submit.html', {'section': 'submit','form': form, 'contest_id': contest_id,
                                                   'is_admin': is_admin(request.user, contest_id),'contestname':contest_name(contest_id),})


@login_required
def submission(request, contest_id):

    if not does_exist_contest(contest_id):
        raise Http404
    if not has_permission_contest(request.user, contest_id):
        raise PermissionDenied

    submissions = get_submissions(request.user,contest_id)
    submission_filter = SubmissionFilterAdmin(request.GET, queryset=submissions)
    submission_filter.form.fields['user'].queryset = get_users_and_admins(contest_id)
    if not is_admin(request.user,contest_id):
        submission_filter = SubmissionFilterUSer(request.GET, queryset=submissions)
    submission_filter.form.fields['problem'].queryset = get_problems(request.user, contest_id)

    return render(request, 'account/submission.html',
                  {'section': 'submission', 'contest_id': contest_id, 'submissions': submissions,
                   'filter': submission_filter, 'is_admin': is_admin(request.user, contest_id), 'enum':Statuses,'contestname':contest_name(contest_id),})


@login_required
def download_submission(request, contest_id, submit_id):

    if not does_exist_submit(contest_id,submit_id):
        raise Http404
    if not has_permission_submit(request.user, contest_id, submit_id):
        raise PermissionDenied

    path = get_object_or_404(Submit, id=submit_id).source.path
    if os.path.exists(path):
        with open(path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="text/x-cf")
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(path)
            return response
    raise Http404


@login_required
def ranking(request, contest_id):
    if not does_exist_contest(contest_id):
        raise Http404
    if not has_permission_contest(request.user, contest_id):
        raise PermissionDenied

    R = get_ranking(contest_id)
    return render(request, 'account/ranking.html', {'section': 'ranking', 'contest_id': contest_id,
                                                   'is_admin': is_admin(request.user, contest_id),
                                                   'contestname': contest_name(contest_id), 'ranking': R })


@login_required
def change_status(request, contest_id, submit_id):

    if not does_exist_submit(contest_id,submit_id):
        raise Http404
    if (not has_permission_submit(request.user, contest_id, submit_id)) or (not is_admin(request.user, contest_id)):
        raise PermissionDenied

    submit = get_object_or_404(Submit, id=submit_id)

    if request.method == 'POST':
        form = ChangeStatusForm(request.POST, request.FILES)
        if form.is_valid():
            new_submit = form.save(commit=False)
            submit.status=new_submit.status
            submit.save(check=False)
            return render(request, 'account/change_status_done.html',
                              {'section': 'submission', 'form': form, 'contest_id': contest_id,
                               'is_admin': is_admin(request.user, contest_id), 'contestname': contest_name(contest_id), 'sub':submit_id})
    else:
        form = ChangeStatusForm(instance=submit)
    return render(request, 'account/change_status.html', {'section': 'submission', 'form': form, 'contest_id': contest_id,
                                                   'is_admin': is_admin(request.user, contest_id),
                                                   'contestname': contest_name(contest_id), 'sub':submit_id })















