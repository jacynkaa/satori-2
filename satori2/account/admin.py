from django.contrib import admin
from .models import *


admin.site.register(Contest)
admin.site.register(Problem)
admin.site.register(ContestUser)
admin.site.register(Submit)

