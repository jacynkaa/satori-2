from .forms import *
from .models import *
from django.db import connection


def get_problems(user, contest_id):
    return Problem.objects.filter(contest=contest_id, visible__in=(True, not bool(ContestUser.objects.filter(contest=contest_id, user=user.id, status='admin'))))


def get_submissions(user, contest_id):
    if bool(ContestUser.objects.filter(contest=contest_id, user=user.id, status='admin')):
        return Submit.objects.filter(problem__in=get_problems(user, contest_id))
    return Submit.objects.filter(problem__in=get_problems(user, contest_id), user=user.id)


def get_contests(user):
    contest_admin = ContestUser.objects.filter(user=user.id, status='admin')
    contest_user = ContestUser.objects.filter(user=user.id, status='user')
    administrated_contests = (c_a.contest.id for c_a in contest_admin)
    user_contests=(c_a.contest.id for c_a in contest_user)

    return Contest.objects.filter(id__in=administrated_contests).union(Contest.objects.filter(id__in=user_contests, visible=True))


def get_users(contest_id): 
    return User.objects.filter(id__in=ContestUser.objects.filter(contest=contest_id, status='user').values('user'))


def get_admins(contest_id): 
    return User.objects.filter(id__in=ContestUser.objects.filter(contest=contest_id, status='admin').values('user'))


def get_users_and_admins(contest_id):
    return User.objects.filter(id__in=ContestUser.objects.filter(contest=contest_id).values('user'))


def get_not_users_nor_admins(contest_id): #
    return User.objects.difference(get_users_and_admins(contest_id))


def is_admin(user, contest_id):   
    return bool(ContestUser.objects.filter(user=user.id, contest=contest_id, status='admin'))


def does_exist_contest(contest_id):
    return bool(Contest.objects.filter(id=contest_id))


def does_exist_problem(contest_id, problem_id):  # if problem exist, but not in this contest return False
    print(contest_id, problem_id)
    return bool(Problem.objects.filter(contest=contest_id, id=problem_id))


def does_exist_submit(contest_id, submit_id): # same here
    if not bool(Submit.objects.filter(id=submit_id)) : return False
    return contest_id==Problem.objects.get(id=Submit.objects.get(id=submit_id).problem.id).contest.id


def has_permission_submit(user, contest_id, submit_id):  # whether user is permitted to see this content or not
    return does_exist_submit(contest_id, submit_id) and has_permission_contest(user, contest_id) and(is_admin(user, contest_id) or Submit.objects.get(id=submit_id).user == user)


def has_permission_problem(user, contest_id, problem_id):
    return does_exist_problem(contest_id, problem_id) and (is_admin(user, contest_id) or (bool(get_users(contest_id).filter(id=user.id)) and Problem.objects.get(id=problem_id).visible))


def has_permission_contest(user, contest_id):
    return does_exist_contest(contest_id) and (is_admin(user,contest_id) or (bool(get_users(contest_id).filter(id=user.id) and Contest.objects.get(id=contest_id).visible)))


def delete_user_from_contest(username,contest_id):
    return ContestUser.objects.get(user=User.objects.get(username=username).id,contest=contest_id).delete()


def contest_name (contestid): #
    return Contest.objects.get(id=contestid).name

def get_ranking(contest_id):
    with connection.cursor() as cursor:
        cursor.execute('select * from contest_ranking(%s)',[contest_id])
        return cursor.fetchall()

