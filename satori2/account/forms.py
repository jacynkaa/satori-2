from django import forms
from django.contrib.auth.models import User
from .models import *
import django_filters  #pip3 install django-filter
import django.contrib.auth.password_validation as validators


class UserRegistrationForm(forms.ModelForm):
    password = forms.CharField(label='Password',
                               widget=forms.PasswordInput)
    password2 = forms.CharField(label='Repeat password',
                                widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')

    def clean_password2(self):
        cd = self.cleaned_data
        print(cd)
        validators.validate_password(password=cd['password'])
        if cd['password'] != cd['password2']:
            raise forms.ValidationError('Passwords don\'t match.')
        return cd['password2']


class ProblemRegistrationForm(forms.ModelForm):

    class Meta:
        model = Problem
        #fields = '__all__'
        exclude = ['contest']


class ContestUserForm(forms.ModelForm):

    class Meta:
        model = ContestUser
        fields = ['user']


class DeleteContestUserForm(forms.ModelForm):

    class Meta:
        model = ContestUser
        fields = ['user']


class SubmitForm(forms.ModelForm):

    class Meta:
        model = Submit
        fields = ['problem', 'source']


class SubmissionFilterAdmin(django_filters.FilterSet):

    class Meta:
        model = Submit
        fields = ['problem', 'user']


class SubmissionFilterUSer(django_filters.FilterSet):

    class Meta:
        model = Submit
        fields = ['problem']


class ChangeStatusForm(forms.ModelForm):

    class Meta:
        model = Submit
        fields = ['status']


'''
class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)
'''

