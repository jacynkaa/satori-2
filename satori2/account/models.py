from django.conf import settings
from django.utils import timezone
from django_enumfield import enum #pip3 install django-enumfield
from django.db import connection, models
from django.core.exceptions import ObjectDoesNotExist
from django.core.exceptions import ValidationError



class Contest(models.Model):

    name = models.CharField(max_length=250, blank=False, unique=True)
    shortname = models.CharField(max_length=10, blank=False, unique=True)
    visible = models.BooleanField(default=True, blank=False)

    class Meta:
        ordering = ('-id',)

    def __str__(self):
        return self.shortname

    objects = models.Manager()


def pathh(self, filename):
    url = "problems/%s/statement.pdf" % self.id
    print(self.id)
    return url


def zip_path(self, filename):

    url = "problems/%s/io.zip" % self.id
    print(self.id)
    return url


def checker_path(self, filename):

    url = "problems/%s/checker.cpp" % self.id
    print(self.id)
    return url


def prefetch_id(instance):
    """ Fetch the next value in a django id autofield postgresql sequence, copy pasted from  https://djangosnippets.org/snippets/2731/ works only for PostgresSQL"""
    cursor = connection.cursor()
    cursor.execute(
        "SELECT nextval('{0}_{1}_id_seq'::regclass)".format(
            instance._meta.app_label.lower(),
            instance._meta.object_name.lower(),
        )
    )
    row = cursor.fetchone()
    cursor.close()
    return int(row[0])


class Problem(models.Model):

    name = models.CharField(max_length=250,  blank=False)
    shortname = models.CharField(max_length=4,  blank=False)
    contest = models.ForeignKey(Contest,  on_delete=models.CASCADE)
    statement = models.FileField( upload_to=pathh, blank=False)
    start = models.DateTimeField(default=timezone.now, blank=False)
    koniec = models.DateTimeField(blank=True, null=True) 
    visible = models.BooleanField(default=False, blank=False)
    time_limit = models.IntegerField(default=1000, blank=False, help_text='in millisecond')
    memory_limit = models.IntegerField(default=512, blank=False, help_text='in MB')
    compilation_time_limit = models.IntegerField(default=10000, blank=False, help_text='in millisecond')
    compilation_memory_limit = models.IntegerField(default=512, blank=False, help_text='in MB')
    code_size_limit = models.IntegerField(default=256, blank=False, help_text='in KB')
    tests = models.FileField(upload_to=zip_path, blank=False)
    checker = models.FileField(upload_to=checker_path, blank=True)

    class Meta:
        ordering = ('-id',)
        constraints = [ models.UniqueConstraint(fields=['contest','name'], name='uniq_name_in_contest'), 
        models.UniqueConstraint(fields=['contest','shortname'], name='uniq_shortname_in_contest') ]

    def __str__(self):
        return self.shortname

    def save(self, *args, **kwargs):
        if not self.id:
            self.id = prefetch_id(self)
        super(Problem, self).save(*args, **kwargs)


class ContestUser(models.Model):
    STATUS_CHOICES = (
        ('user', 'User'),
        ('admin', 'Admin'),
    )

    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='user')
    contest = models.ForeignKey(Contest,  on_delete=models.CASCADE, blank=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, blank=False)

    class Meta:
        constraints = [ models.UniqueConstraint(fields=['contest','user'], name='unique_contest_user')]

    def __str__(self):
        return self.user.__str__()


def submit_path(self, filename):
    url = "submit/%s/source.cpp" % self.id
    print(self.id )
    return url


class Statuses(enum.Enum):
    OK = 1
    ANS = 2
    RTE = 3
    TLE = 4
    MEM = 5
    CME = 6
    QUE = 7
    INT = 8
    REJ = 9

    labels = {
            OK : 'OK',
            ANS : 'ANS',
            RTE : 'RTE',
            TLE : 'TLE',
            MEM : 'MEM',
            CME : 'CME',
            QUE : 'QUE',
            INT : 'INT',
            REJ : 'REJ'
    }


class Submit (models.Model):

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, blank=False)
    problem = models.ForeignKey(Problem,  on_delete=models.CASCADE)
    source = models.FileField(upload_to=submit_path, blank=False)
    submit_time = models.DateTimeField(default=timezone.now, blank=False)
    status = enum.EnumField(Statuses, default=Statuses.QUE)

    class Meta:
        ordering = ('-id',)

    def __str__(self):
        return str(self.id)

    def clean(self):
        try :
            pr = Problem.objects.get(id=self.problem.id)
            if ContestUser.objects.get(user=self.user,contest=pr.contest).status!='admin' :
                if self.submit_time < pr.start: 
                    raise ValidationError('You are not allowed to submit for this problem now')

                if (not pr.koniec is None) and (self.submit_time > pr.koniec): 
                    raise ValidationError('You are not allowed to submit for this problem now')

        except ObjectDoesNotExist as exc: pass

    def save(self, check=True, *args, **kwargs):
        if check is True:
            self.clean()

        if not self.id:
            self.id = prefetch_id(self)

        super(Submit, self).save(*args, **kwargs)
