#/usr/bin/env/python3

import sys
import subprocess
import os
from .models import *

def sub(new_submit):    
    subprocess.run(['mkdir','tmp/satori2_submits/'+str(new_submit.id),'-p'])
    
    try:
        zad = Problem.objects.get(id=new_submit.problem.id)
        stat = 8 
        ch = None

        try : 
            ch = zad.checker.path
        except : 
            ch = None 

        stat = test(new_submit.source.path, zad.tests.path, zad.time_limit, zad.memory_limit, zad.compilation_time_limit, zad.code_size_limit, zad.compilation_memory_limit, ch, new_submit.id)

        new_submit.status=stat
        new_submit.save(update_fields=["status"])
        subprocess.run(['rm','-r','tmp/satori2_submits/'+str(new_submit.id)])

    except : 
        new_submit.status=8
        new_submit.save(update_fields=["status"])
        subprocess.run(['rm','-r','tmp/satori2_submits/'+str(new_submit.id)])


def test(source, io_zip, time_limit, memory_limit, c_time_limit, source_size_limit, c_memory_limit, checker, sb_id) : 

        try:
                source_name = source.split('.')[0].split('/')[-1]

                # compilation

                if checker != None :
                        try:
                                ch_comp_cp = subprocess.run(['g++',checker,'-o',"tmp/satori2_submits/"+ str(sb_id) + "/checker.e",'--std=c++14','-O2','-static'],timeout=30)

                                if ch_comp_cp.returncode != 0 : 
                                        print('INT')
                                        return 8

                        except subprocess.TimeoutExpired : 
                                print('INT')
                                return 8


                if os.path.getsize(source) > source_size_limit * 1024 : 
                        print('CME')
                        return 6

                try:
                        with open('tmp/satori2_submits/'+ str(sb_id) +'/mem_out','w+') as memout:
                                 compilation_cp = subprocess.run(['/usr/bin/time','-f','%M %U','g++',source,'-o','tmp/satori2_submits/' + str(sb_id) + '/' + source_name+".e",'--std=c++14','-O3','-static'], timeout=c_time_limit*0.001 + 10, stderr=memout)

                        if compilation_cp.returncode != 0 : 
                                print('CME')
                                return 6

                        with open('tmp/satori2_submits/' + str(sb_id) + '/mem_out','r') as memout :
                                R = memout.read()
                                R1 = R.split(' ')[-2].split('\n')[-1]
                                R2 = R.split(' ')[-1]

                                if int(R1) > c_memory_limit * 1024 or float(R2) * 1000 > c_time_limit :
                                        print('CME')
                                        return 6


                except subprocess.TimeoutExpired : 
                        print('CME')
                        return 6

                print('Compilation successful')

                # unzipping

                in_files = list()
                all_files = list()
                with open('tmp/satori2_submits/'+ str(sb_id) +'/zip_out','w+') as zipout :
                        subprocess.run(['unzip','-o',io_zip,'-d','tmp/satori2_submits/' + str(sb_id)], stdout=zipout)
                
                with open('tmp/satori2_submits/'+str(sb_id)+'/zip_out','r') as zipout : 
                        R = zipout.readlines()
                        for z in R : 
                                words = z.split(' ')
                                for ext_file in words :
                                        if ext_file.split('.')[-1] == 'in' :
                                                in_files.append(ext_file)
                                                all_files.append(ext_file)
                                        if ext_file.split('.')[-1] == 'out' : 
                                                all_files.append(ext_file)

                # solution run
                subprocess.run(['ulimit'+' -s '+str(2*memory_limit)],shell=True)

                for num,t_in in enumerate(in_files) :
                        print('test ' + str(num) + ' :',end=' ')

                        try :
                                with open('tmp/satori2_submits/'+str(sb_id)+'/prog_out', 'w+') as prog_out, open(t_in, 'r+') as test_input,open('tmp/satori2_submits/'+str(sb_id)+'/mem_out','w+') as memout :  
                                        solve_cp = subprocess.run(['/usr/bin/time','-f','%M %U','tmp/satori2_submits/' +str(sb_id)+ '/' +source_name+'.e'], timeout = time_limit*0.001 + 10, stdin=test_input, stdout=prog_out, stderr = memout)                               

                                        if solve_cp.returncode != 0 : 
                                                print('RTE')
                                                return 3 

                                with open('tmp/satori2_submits/'+str(sb_id)+'/mem_out','r') as memout :

                                        R = memout.read()
                                        R1 = R.split(' ')[-2].split('\n')[-1]
                                        R2 = R.split(' ')[-1]

                                        print(R2[:-1],end='  ')

                                        if int(R1) > memory_limit * 1024  :
                                                print('MEM')
                                                return 5               

                                        if float(R2) * 1000 > time_limit  :
                                                print('TLE')
                                                return 4

                        except subprocess.TimeoutExpired :
                                print(str(time_limit)[0]+'.'+str(time_limit)[1:3],end='  ')
                                print('TLE')
                                return 4

                # checking

                        if checker == None :
                                with open('/dev/null','w') as dev_null :
                                    diff_cp = subprocess.run(['diff','-w','-B',t_in.split('.')[0]+'.out','tmp/satori2_submits/'+str(sb_id)+'/prog_out'],stdout=dev_null) 
                                    if diff_cp.returncode != 0 : 
                                            print('ANS')
                                            return 2

                        else :
                                with open('/dev/null','w') as dev_null :                 # in, hint, out
                                    check_cp = subprocess.run(['tmp/satori2_submits/'+str(sb_id)+'/checker.e',t_in,t_in.split('.')[0]+'.out','tmp/satori2_submits/'+str(sb_id)+'/prog_out'],stdout=dev_null)
                                    if check_cp.returncode !=0 :
                                        print('ANS')
                                        return 2

                        print('OK')
                                

        except :
                print('INT')
                return 8

        return 1
