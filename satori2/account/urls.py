from django.urls import path
from django.contrib.auth import views as auth_views
from . import views




urlpatterns = [


    path('', views.dashboard, name='dashboard'),
    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),

    # reset password urls TBD
   # path('password_reset/', auth_views.PasswordResetView.as_view(), name='password_reset'),
   # path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    #path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    #path('reset/done/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),

    # change password urls
    path('password_change/', auth_views.PasswordChangeView.as_view(), name='password_change'),
    path('password_change/done/', auth_views.PasswordChangeDoneView.as_view(),  name='password_change_done'),

    # register new user
    path('register/', views.register, name='register'),

    path('<int:contest_id>/', views.contest_detail, name='contest_detail'),
    path('<int:contest_id>/problem/', views.problem, name='problem'),
    path('<int:contest_id>/<int:problem_id>/download_problem/', views.download_problem, name='download_problem'),
    path('<int:contest_id>/add_problem', views.add_problem, name='add_problem'),
    path('<int:contest_id>/add_contest_user', views.add_contest_user, name='add_contest_user'),
    path('<int:contest_id>/submit', views.submit, name='submit'),
    path('<int:contest_id>/submission', views.submission, name='submission'),
    path('<int:contest_id>/<int:submit_id>/download_submission/', views.download_submission, name='download_submission'),
    path('<int:contest_id>/<int:submit_id>/change_status/', views.change_status, name='change_status'),
    path('<int:contest_id>/<int:problem_id>/edit_problem/', views.edit_problem, name='edit problem'),
    path('<int:contest_id>/ranking/', views.ranking, name='ranking'),


]