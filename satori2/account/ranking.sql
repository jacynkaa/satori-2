create or replace function get_submissions(contest_id int,user_id int, problem_id int )
  returns table (id int, submit_time TIMESTAMP(0), status int)
as
$$
  SELECT a_s.id, submit_time::TIMESTAMP(0),status FROM account_submit a_s JOIN account_problem a_p ON(a_s.problem_id=a_p.id)
  WHERE get_submissions.contest_id=a_p.contest_id AND (a_s.user_id=get_submissions.user_id OR get_submissions.user_id=0) AND (a_s.problem_id=get_submissions.problem_id OR get_submissions.problem_id=0)
  ORDER BY 1;
$$
language sql;

CREATE or REPLACE FUNCTION contest_ranking(cont_id int) returns
table (
	name text,
	score int,
	penalty int,
	problems text
)
as $$
begin
        return query     
        select username::text as name,(count(distinct case when S.status=1 then problem_id else null end))::int as score,
	sum(case when S.status>1 and S.status<6 and has_ok then 1 else 0 end)::int as penalty,
	string_agg(distinct case when S.status=1 then P.shortname||'['||problem_tries||'] ' else '' end,'')
	from get_submissions(cont_id,0,0) join account_submit S using(id) join auth_user SU on (S.user_id=su.id)
	join account_problem P on(S.problem_id=P.id) join account_contestuser CU on (cont_id=CU.contest_id and SU.id=CU.user_id)
	join (select S2.problem_id as p_id, user_id,
 	sum(case when S2.status>1 and S2.status<6 then 1 else 0 end)+1 as problem_tries, min(S2.status)=1 as has_ok 
	from get_submissions(cont_id,0,0) join account_submit S2 using(id)
	where S2.problem_id = problem_id group by S2.user_id, S2.problem_id) PP on(PP.user_id=S.user_id and PP.p_id=problem_id)
	where CU.status='user' group by SU.id, first_name, last_name order by score desc, penalty;
end; $$
language plpgsql;
