#include <bits/stdc++.h>
typedef long long ll;
using namespace std;
const int N=1e6+100;

ll P[N];
ll il[N];

int main()
{
	ll n,M,A,B;
	scanf("%lld%lld%lld%lld",&n,&M,&A,&B);

	P[0]=A;
	for(int i=1;i<=M-1;i++) P[i]=(P[i-1]*B)%M;

	for(int i=0;i<=n-1;i++)
	{
		for(int j=i+1;j<=n-1;j++) 
		{
			if(__builtin_popcount(P[(i*n+j)%(M-1)])%2==1) il[i]++;
			else il[j]++;
		}
	}

	ll res=0;

	for(int i=0;i<=n-1;i++) res+=(il[i]-1)*il[i]/2;

	printf("%lld\n",n*(n-1)*(n-2)/6-res);
}
