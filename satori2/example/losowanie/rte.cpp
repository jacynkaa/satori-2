#include <bits/stdc++.h>
typedef long long ll;
using namespace std;
const int N=1e6+6;

ll P[N];
ll prefA[N];
ll odn[N];

ll sumA(ll a,ll b);
ll sumB(ll x,ll il_R, ll M);

vector<ll> prefB[N];

int main()
{
	ll n,M,A,B;
	scanf("%lld%lld%lld%lld",&n,&M,&A,&B);

	ll L=3*(M-1);
	while(L<=3*n) L+=(M-1);

	P[0]=A;
	for(ll i=1;i<=L;i++) P[i]=(P[i-1]*B)%M;

	prefA[0]=__builtin_popcountll(P[0])%2;
	for(ll i=1;i<=L;i++) prefA[i]=prefA[i-1]+__builtin_popcountll(P[i])%2;

	ll il_R=__gcd(n,M-1);
	for(ll i=0;i<il_R;i++)
	{
		ll r=i;
		odn[i]=0;
		prefB[i].push_back(__builtin_popcountll(P[r])%2);
		
		ll j=1;
		r=(r+n)%(M-1);
		while(r!=i)
		{
			odn[r]=j;
			prefB[i].push_back(prefB[i][j-1]+__builtin_popcountll(P[r])%2);
			r=(r+n)%(M-1);
			j++;
		}
	}

	ll bad_ones=0;
	for(ll i=0;i<=n-1;i++)
	{
		ll il=0;
		ll a = (i*n+i+1)%(M-1);
		ll b = a+(n-i-2);

		if(i<n-1) il+=sumA(a,b);
		if(i>0) il+=(i-sumB(i,il_R,M));

		bad_ones+=((il-1)*il)/2;
	}

	printf("%lld\n",n*(n-1)*(n-2)/6-bad_ones);
}
//////
ll sumA(ll a,ll b)
{
	if(a==0) return prefA[b];
	return prefA[b]-prefA[a-1];
}
//////
ll sumB(ll x,ll il_R,ll M)
{
	ll r=x%il_R;
	ll p=odn[x%(M-1)];

	if(p+x<=(ll)prefB[r].size()) 
	{
		if(p>0) return prefB[r][p+x-1]-prefB[r][p-1];
		return prefB[r][p+x-1];
	}

	ll res=0;
	if(p>0) res+=prefB[r].back()-prefB[r][p-1];
	else res+=prefB[r].back();
	x-=((ll)prefB[r].size()-p);

	res+=(x/(ll)prefB[r].size())*prefB[r].back();
	x%=(ll)prefB[r].size();

	if(x==0) return res;
	return res+prefB[r][x-1];
}
