#include <bits/stdc++.h>
#define F(i,a,b) for(int i=a;i<=b;i++)
using namespace std;
const int N=5e5+5;

char S[N];

 int main()
 {
	int n;
	scanf("%d",&n);
	scanf("%s",(S+1));

	int mx=3;

	F(i,1,n-mx+1)
	{
		int sum=0;

		F(j,0,n-i)
		{
			if(sum==3) break;
			sum+=(int)(S[i+j]=='1');
			mx=max(mx,j+1);
			if(j==n-i&&sum<3) mx=max(mx,n-i+2);
		}
	}

	printf("%d\n",mx);
 }
