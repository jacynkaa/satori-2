#include <bits/stdc++.h>
typedef long long ll;
#define F(i,a,b) for(ll i=a;i<=b;i++)
#define Fd(i,a,b) for(ll i=a;i>=b;i--)
using namespace std;
const int N=1e6+6;

int A[N];
bool check(int n,int v);

 int main()
 {
        int n;
        scanf("%d\n",&n);

        F(i,1,n)
        {
                char c;
                scanf("%c",&c);
                A[i] = int(c == '1');
        }

        int a,b,v;
        a=3;
        b=n;

        while(a<b)
        {
                v=(a+b)/2;

                if(check(n,v)) b = v;
                else a = v+1;
        }

        printf("%d\n",a);
 }
////////
 bool check(int n,int v)
 {
        int sum = 0;
        F(i,1,v) sum+=A[i];
        if(sum<3) return false;

        F(i,v+1,n)
        {
                sum+=(A[i]-A[i-v]);
                if(sum<3) return false;
        }

        return true;
 }
