#include <bits/stdc++.h>
typedef long long ll;
#define F(i,a,b) for(ll i=a;i<=b;i++)
#define Fd(i,a,b) for(ll i=a;i>=b;i--)
using namespace std;
const int N=(1e5)+5;

int H[N];
int O[N];
vector<int> G[N];
vector<int> wn;

void df(int v,int oj,int h);
void path(int v,int oj);

 int main()
 {
	int n;
	scanf("%d",&n);
	int a,b;
	F(i,2,n)
	{
		scanf("%d%d",&a,&b);
		G[a].push_back(b);
		G[b].push_back(a);
	}

	int S,T;

	df(1,0,0);
	T=max_element(H+1,H+1+n)-H;
	df(T,0,0);
	S=max_element(H+1,H+1+n)-H;

	bool bl[N];
	F(i,1,n) bl[i]=false;

	while(S!=0)
	{
		bl[S]=true;
		wn.push_back(S);

		for(auto z : G[S])
		{
			if((z!=O[S])&&(!bl[z])) path(z,S), wn.push_back(S);
		}

		S=O[S];
	}	

	printf("%d\n",(int)wn.size());
	for(auto z : wn) printf("%d ",z);
	printf("\n");
 }
///////
 void df(int v,int oj,int h)
 {
	O[v]=oj;
	H[v]=h;

	for(auto z : G[v])
	{
		if(z==oj) continue;
		df(z,v,h+1);
	}
 }
/////
 void path(int v,int oj)
 {
	wn.push_back(v);

	for(auto z : G[v])
	{
		if(z==oj) continue;
		path(z,v);
		wn.push_back(v);
	}
 }

