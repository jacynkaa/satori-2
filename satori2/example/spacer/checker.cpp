#include <bits/stdc++.h>
using namespace std;

set<pair<int,int>> st;

int main(int argc, char ** argv)
{
    FILE* in = fopen(argv[1],"r");
    FILE* hint = fopen(argv[2],"r");
    FILE* out = fopen(argv[3],"r");

    int n;
    fscanf(in,"%d",&n);
    for(int i=1;i<n;i++)
    {
	    int a,b;
	    fscanf(in,"%d%d",&a,&b);
	    st.insert(make_pair(a,b));
	    st.insert(make_pair(b,a));
    }

    int ans_k;
    int k;
    fscanf(hint,"%d",&ans_k);
    fscanf(out,"%d",&k);
    if(k!=ans_k) 
    {
	    printf("Length\n");
	    exit(1);
    }
    
    char c;
    fscanf(out,"%c",&c);
    if(c!='\n')
    {
	    printf("Format: endl \n"); 
	    exit(1);
    }

    bool bl[100005];
    for(int i=1;i<=n;i++) bl[i]=false;
    vector<int> path;
    for(int i=1; i<=k; i++)
    {
	    int a;
	    fscanf(out,"%d",&a);
	    fscanf(out,"%c",&c);
	    if(i<k && c!=' ')
	    {
		    printf("Format: spaces\n");
		    exit(1);
	    }
	    path.push_back(a);
	    bl[a]=true;
	    if(!( a<=n && a>=1 ))
	    {
		    printf("Bounds\n");
		    exit(1);
	    }
    }

    for(int i=1;i<=n;i++)
    {
	if(!bl[i])
	{
		printf("Visited\n");
		exit(1);
	}
    }	

    for(int i=1;i<k;i++)
    {
	if(st.count(make_pair(path[i],path[i-1])) == 0)
	{	
		printf("Adjacent\n");
		exit(1);
	}
    }

    fclose(in);
    fclose(hint);
    fclose(out);

    printf("OK\n");
    exit(0);
}

