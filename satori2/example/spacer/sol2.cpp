#include<bits/stdc++.h>
using namespace std;
#define int long long 
#define mp make_pair
#define pb push_back
#define pii pair<int,int>
#define F first
#define S second
int const M=2e5+10,M3=1e3+10,M2=1e6+10;
pii maxx[M];
vector<int>adj[M];
int v,root,ans,good[M],dep[M],leaf[M];
vector<int>an;
void relax(pii &ma,int x)
{
	if(x>ma.F)ma.S=ma.F,ma.F=x;
	else if(x>ma.S)ma.S=x;
}
void dfs(int v,int l,int h)
{
	int mx=0,tmp=0;
	for(int i=0;i<adj[v].size();i++)
	{
		int u=adj[v][i];
		if(u==l)continue;
		dfs(u,v,h+1);
		if(mx<maxx[u].F+1)tmp=u,mx=maxx[u].F+1;
		relax(maxx[v],maxx[u].F+1);
	}
	if(adj[v].size()==1)leaf[v]=v;
	else
		leaf[v]=leaf[tmp];
	if(maxx[v].F+maxx[v].S>ans)ans=maxx[v].F+maxx[v].S,root=leaf[v];
}
void dfs2(int v,int l)
{
	for(int i=0;i<adj[v].size();i++)
	{
		int u=adj[v][i];
		if(u==l)continue;
		dfs2(u,v);
		if(dep[u]+1>=dep[v])dep[v]=dep[u]+1,good[v]=u;
	}
}
void dfs3(int v,int l,bool ch)
{
	for(int i=0;i<adj[v].size();i++)
	{
		int u=adj[v][i];
		if(u==l)continue;
		if(u==good[v])continue;
		an.pb(u);
		dfs3(u,v,0);
	}
	if(good[v]!=0)
	{
		an.pb(good[v]);
		bool flag=1;
		if(ch==0)flag=0;
		dfs3(good[v],v,flag);
	}
	if(!ch)an.pb(l);
}
main()
{
	int n,a,b;
	cin>>n;
	for(int i=1;i<=n-1;i++)cin>>a>>b,adj[a].pb(b),adj[b].pb(a);
	dfs(1,0,1);
	dfs2(root,0);
	an.pb(root);
	dfs3(root,0,1);
	cout<<(int)an.size()<<endl;
	for(int i=0;i<an.size();i++)cout<<an[i]<<" ";
}
