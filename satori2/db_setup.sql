create database satori2;
create user satori2 with encrypted password 'satori-2019';
grant all privileges on database satori2 to satori2;